import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(home: Home());
  }
}

class Home extends StatelessWidget {
  final Color? surfaceTintColor;

  const Home({this.surfaceTintColor, super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Eu Sou Rico'),
        centerTitle: true,
        backgroundColor: Colors.blueGrey[900],
        bottomOpacity: 0.5,
        titleTextStyle: const TextStyle(
            color: Colors.white,
            // ! fontFamily: 'American-Typewriter',
            fontSize: 24,
            fontWeight: FontWeight.bold),
      ),
      //
      backgroundColor: Colors.blueGrey,
      //
      body: const Center(
        child: Center(
          child: Image(
            image: AssetImage('assets/image/rubi.png'),
            width: 350.0,
            height: 300.0,
            // ! alignment: Alignment.center,
          ),
        ),
      ),
    );
  }
}
